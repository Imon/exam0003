 <?php
        
        include_once ("src\pencil.php");
         include_once ("src\house.php");
         include_once ("src\car.php");
         include_once ("src\book.php");
        
        use src\bitm\pencil;
        use src\bitm\house;
        use src\bitm\car;
        use src\bitm\book;
        
         $info_pencil=new pencil();
         $info_pencil->sayAbout();
         
         $info_house=new house();
         echo "<hr \>";
         $info_house->infoHouse();
         echo "<hr \>";
         $info_car=new car();
         $info_car->infoCar();
         echo "<hr \>";
         $info_book=new book();
         $info_book->infoBook();
         echo "<hr \>";
        
        ?>
